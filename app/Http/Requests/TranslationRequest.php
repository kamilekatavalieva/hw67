<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TranslationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize():bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules():array
    {
        return [
            'ru_translator' => ['required'],
        ];
    }

    /**
     * @return string[]
     */
    public function messages():array
    {
        return [
            'translator.required' => ':Attribute поле должно быть заполнено',
        ];
    }
}
