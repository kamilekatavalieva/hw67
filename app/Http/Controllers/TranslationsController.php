<?php

namespace App\Http\Controllers;

use App\Http\Requests\TranslationRequest;
use App\Models\Translator;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TranslationsController extends Controller
{


    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $translators = Translator::all();
        return view('translators.index', compact('translators'));

    }


    /**
     * @param TranslationRequest $request
     * @return RedirectResponse
     */
    public function store(TranslationRequest $request):RedirectResponse
    {
        $translation = new Translator();

        $user = Auth::user();
        $data = [ 'user_id' => $user->id,
            'ru' =>
                [
                    'translation' => $request->input('ru_translator'),
                ]
        ];

        $translation->create($data);

        return back();
    }


    /**
     * @param Translator $translator
     * @return Application|Factory|View
     */
    public function show(Translator $translator)
    {
        return view('translators.show', compact('translator'));
    }



    /**
     * @param Request $request
     * @param Translator $translator
     * @return RedirectResponse
     */
    public function update(Request $request, Translator $translator):RedirectResponse
    {
        if(!empty($request->input('en_translator'))) {
            $data = [
                'en' =>
                    [
                        'translation' => $request->input('en_translator'),
                    ]
            ];
            $translator->update($data);
        }
        if(!empty($request->input('uz_translator'))) {
            $data = [
                'uz' =>
                    [
                        'translation' => $request->input('uz_translator'),
                    ]
            ];
            $translator->update($data);
        }
        if(!empty($request->input('tat_translator'))) {
            $data = [
                'tat' =>
                    [
                        'translation' => $request->input('tat_translator'),
                    ]
            ];
            $translator->update($data);
        }
        if(!empty($request->input('ky_translator'))) {
            $data = [
                'ky' =>
                    [
                        'translation' => $request->input('ky_translator'),
                    ]
            ];
            $translator->update($data);
        }

        return back();
    }

}
