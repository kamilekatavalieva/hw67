<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Translator extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    /**
     * @var string[]
     */
    public $translatedAttributes = ['translation'];

    /**
     * @var string[]
     */
    protected $fillable = ['user_id'];


    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
