@extends('layouts.app')
@section('content')
    <div class="container">
        <dv>
            <h3>@lang('messages.translate') "{{$translator->translate('ru')->translation}}"</h3>
        </dv>
        @if(Auth::check())
            <form method="post" action="{{route('translators.update', ['translator' => $translator])}}">
                @method('put')
                @csrf
                <input type="hidden" id="translation_id" value="{{$translator->id}}">
                @if(!empty($translator->translate('ru')->translation))
                    <h4>{{$translator->translate('ru')->translation}}</h4>
                @else
                    <div class="form-group">
                        <label for="ru_translation">@lang('messages.russian')</label>
                        <input name="ru_translation"  type="text" class="form-control"
                               id="ru_translation">
                    </div>
                @endif
                @if(!empty($translator->translate('en')->translation))
                    <h4>{{$translator->translate('en')->translation}}</h4>
                @else
                    <div class="form-group">
                        <label for="en_translation">@lang('messages.english')</label>
                        <input name="en_translation"  type="text" class="form-control"
                               id="en_translation">
                    </div>
                @endif
                @if(!empty($translator->translate('uz')->translation))
                    <h4>{{$translator->translate('uz')->translation}}</h4>
                @else
                    <div class="form-group">
                        <label for="uz_translation">@lang('messages.uzbek')</label>
                        <input name="uz_translation" type="text" class="form-control"
                               id="uz_translation">
                    </div>
                @endif
                @if(!empty($translator->translate('tat')->translation))
                    <h4>{{$translator->translate('tat')->translation}}</h4>
                @else
                    <div class="form-group">
                        <label for="tat_translation">@lang('messages.tatar')</label>
                        <input name="tat_translation" type="text" class="form-control"
                               id="tat_translation" >
                    </div>
                @endif
                @if(!empty($translator->translate('ky')->translation))
                    <h4>{{$translator->translate('ky')->translation}}</h4>
                @else
                    <div class="form-group">
                        <label for="ky_translation">@lang('messages.kyrgyz')</label>
                        <input name="ky_translation" type="text" class="form-control"
                               id="ky_translation" >
                    </div>
                @endif
                <button type="submit" class="btn btn-primary">@lang('messages.save_translation')</button>
            </form>
        @else
            @if(!empty($translator->translate('ru')->translation))
                <div>
                    <b>@lang('messages.translate_ru')</b><br>
                    <h5>
                        {{$translator->translate('ru')->translation}}
                    </h5>
                </div>
            @endif

            @if(!empty($translator->translate('en')->translation))
                <div>
                    <b>@lang('messages.translate_en')</b><br>
                    <h5>
                        {{$translator->translate('en')->translation}}
                    </h5>
                </div>
            @endif

            @if(!empty($translator->translate('uz')->translation))
                <div>
                    <b>@lang('messages.translate_uz')</b><br>
                    <h5>
                        {{$translator->translate('uz')->translation}}
                    </h5>
                </div>
            @endif
            @if(!empty($translator->translate('tat')->translation))
                <div>
                    <b>@lang('messages.translate_tat')</b><br>
                    <h5>
                        {{$translator->translate('tat')->translation}}
                    </h5>
                </div>
            @endif
            @if(!empty($translator->translate('ky')->translation))
                <div>
                    <b>@lang('messages.translate_ky')</b><br>
                    <h5>
                        {{$translator->translate('ky')->translation}}
                    </h5>
                </div>
            @endif
        @endif
    </div>

@endsection

