@extends('layouts.app')
@section('content')
    <div class="container" >
        <h3>
            @lang('messages.translations')
        </h3>
        <div>
            @foreach($translators as $translator)
                <h5><a href="{{route('translators.show', ['translator' => $translator])}}">{{$translator->translate('ru')->translation}}</a></h5>
            @endforeach
        </div>
        @if(Auth::check())
            <div>
                <h5>
                    @lang('messages.create_new_translation')
                </h5>

                <form method="post" action="{{route('translators.store')}}">
                    @csrf

                    <div class="form-group">
                        <label for="ru_translator">@lang('messages.translation')</label>
                        <input name="ru_translator" type="text" class="form-control border-success  @error('translator') is-invalid border-danger @enderror"
                               id="ru_translator">
                    </div>
                    @error('translator')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror

                    <button type="submit" class="btn btn-primary">@lang('messages.save_translation')</button>
                </form>
            </div>
        @endif
    </div>
@endsection

