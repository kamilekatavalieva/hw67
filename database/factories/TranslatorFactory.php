<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TranslatorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition():array
    {
        $en_faker = \Faker\Factory::create('en_EN');
        $uz_faker = \Faker\Factory::create('uz_UZ');
        $tat_faker = \Faker\Factory::create('tat_TAT');
        $ky_faker = \Faker\Factory::create('ky_KY');
        return [
            'ru' => [
                'translation' => 'RU - '. $this->faker->sentence() . 'RU',
            ],
            'en' => [
                'translation' => 'EN -' .$en_faker->sentence() . 'EN',
            ],
            'uz' => [
                'translation' =>  'UZ - '.$uz_faker->sentence(). 'UZ',
            ],
            'tat' => [
                'translation' =>  'TAT - '.$tat_faker->sentence(). 'TAT',
            ],
            'ky' => [
                'translation' =>'KY - '. $ky_faker->sentence(). 'KY',
            ],
            'user_id' => rand(1, 5)
        ];
    }
}
